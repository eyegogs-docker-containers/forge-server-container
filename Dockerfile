FROM archlinux:latest

ARG version
ARG command
ARG java

RUN pacman -Syyu --noconfirm && pacman -S --noconfirm wget git make gcc jre${java}-openjdk

WORKDIR /
RUN git clone https://github.com/Tiiffi/mcrcon.git
WORKDIR /mcrcon
RUN make && make install

RUN mkdir /server
WORKDIR /server

RUN wget https://maven.minecraftforge.net/net/minecraftforge/forge/${version}/forge-${version}-installer.jar
RUN java -jar forge-${version}-installer.jar --installServer

RUN echo "eula=true" > eula.txt && echo "${command}" > run_server.sh && chmod +x ./run_server.sh

EXPOSE 25565

CMD /server/run_server.sh
